# Référence Documentaire sur la Calligraphie

* La Calligraphie Facile ; Diana Hardy Wilson ; les éditions de Saxe
* L’ABC du Calligraphe ; David Harris ; Édition Dessain & Tolra
* Guide pratique des Lettres Anluminées ; Timothy Noad & Patricia Seligman ; Édition Dessain & Tolra
* Le Lettrage de A à Z ; Abbey Sy ; édition Vigot
* La Calligraphie Moderne ; Cecilia Skogh ; les éditions de Saxe



