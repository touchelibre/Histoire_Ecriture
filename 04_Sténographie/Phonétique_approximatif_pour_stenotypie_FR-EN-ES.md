> Lilian Tribouilloy ; 13 mai 2019

# Phonétique Approximative pour construire une Sténotypie Française Anglaise et Espagnol

## Approximation Phonétique

### Diagramme API des Voyelles

i•y         ɨ•ʉ      ɯ•u
  \  I•Y     |     ʊ  |
  e•ø       ɘ•ɵ      ɤ•o
    \        ə        |
    ε•œ     ɜ•ɞ      ʌ•ɔ
     æ\      ɐ        |
      a•Œ    |       ɑ•ɒ


### Français Approximatif

a ≈ a ɑ
e ≈ e ε
i = i
o = o ɔ
u = u
ə = ə ø œ
y = y
ã = ã
ẽ = ~ε ~œ
õ = õ

**Remarques :**
* aeiouə	idem anglais
* aeiou		idem espagnol
* yãẽõ		uniquement en français

ã, ẽ, õ peuvent être remplacer par â, ê, ô car le «^» est plus accessible sur le clavier Kéa.


### Rapprochement Français/English

a ≈ ɑː æ
e ≈ e ε
i ≈ iː I
u ≈ uː ʊ
o ≈ ɔː ɒ
ə ≈ ɜː ʌ ə


### Semi‑voyelle et diphtongue

En français les (quasi)diphtongues sont construites avec des semi‑voyelles.
j ≈ i
w ≈ u
ɥ ≈ y

**Remarque :** En espagnol, il existe ʎ ≈ j

En anglais, il y a de vrais diphtongues.
* Finisant en I : 		aI eI ɔI 	≈ ai ei oi
* Finisant en OU : 		əʊ aʊ 		≈ əu au
* Finisant en ER (ɚ) : 	Iɚ eɚ ʊɚ 	≈ iər eər uər (moins marquer chez les américains)

**Remarque :** Les voyelles longues peuvent être utilisé à la place d’une V+r. _Ex: ɜː à la place de ɜr ; ɑː ≈ ar ; ɔː ≈ or_ (moins marquer chez les américains)


### Consonnes

pb td kg

fv θð sz ʃʒ

nm ɲŋ

l

k ≈ c

b ≈ b β

r ≈ r ɹ ʁ χ

h




### Syllabes

n×C + (H) + V + (T) + k×C

____________________________________________________________________________________________________


## Recherche de clavier

### Paradigme de départ
1. Il faut prendre en compte tous les sons existants en français, anglais et espagnol selon l’approximation décrite dans le premier §.
2. Il faut des consonnes initiales et finales pour écrire rapidement des syllabes.


### Inventaire des «multiconsonnes» existantes

pr br tr dr kr gr

pl bl kl gl

st ts

dz

ks gz

str

pn


### Raccourcis Clavier

b = p + *
d = t + *
g = k + *
v = f + *
ð = θ + *
z = s + *
ʒ = ʃ + *

ã = a + *
õ = o + *
ẽ = i + *
ə = e + *
y = u + *


















