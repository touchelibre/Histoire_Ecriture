# Recherche Documentaire sur l’Histoire de l’Écriture

Ce dossier donne ma recherche documentaire sur l’histoire de l’écriture sous divers aspects, allant de l’archéologie au techniques modernes :

* Proto-Écriture
* Calligraphie
* Typographie
* Dactilographie
* Sténographie
* Phonétique
* Télégraphie
* Informatisation
* Ergonomie


